@extends('layouts.app')

@section('content')
<div class="container-fluid h-100">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card mt-5 shadow rounded">
                <div class="card-header text-center p-4">
                    <h5 class="card-title font-weight-bold mb-0">
                        <i class="fas fa-user-tie"></i>
                        Employee Management
                    </h5>
                </div>
                <div class="card-body p-3">
                    <div class="col-12 text-right">
                        <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#createModal">
                            <i class="fas fa-plus"></i>
                            Register new Employee
                        </button>
                    </div>
                    <div class="table-responsive mt-3">
                        <table id="employeeTable" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Department</th>
                                    <th scope="col">First Name</th>
                                    <th scope="col">Middle Name</th>
                                    <th scope="col">Last Name</th>
                                    <th scope="col">Last Access</th>
                                    <th scope="col">Total Access</th>
                                    <th scope="col">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($allEmployees) > 0)
                                    @foreach ($allEmployees as $employee)
                                        <tr>
                                            <td>{{$employee->id}}</td>
                                            <td>{{$employee->department}}</td>
                                            <td>{{$employee->first_name}}</td>
                                            <td>{{$employee->middle_name}}</td>
                                            <td>{{$employee->last_name}}</td>
                                            <td>{{$employee->lastAccess}}</td>
                                            <td>{{$employee->countAccess}}</td>
                                            <td>
                                                <button type="button" id="historyEmployee" class="btn btn-primary" data-toggle="modal" 
                                                        data-target="#historyModal" id-selected="{{$employee->id}}">
                                                        <i class="fas fa-history"></i>
                                                </button>
                                                @if ($employee->canAccess)
                                                    <button type="button" id="lockEmployee" class="btn btn-danger" 
                                                    id-selected="{{$employee->id}}">
                                                        <i class="fas fa-lock"></i>
                                                    </button>
                                                @else
                                                    <button type="button" id="unlockEmployee" class="btn btn-success" 
                                                    id-selected="{{$employee->id}}">
                                                        <i class="fas fa-lock-open"></i>
                                                    </button>
                                                @endif
                                                <button type="button" id="updateEmployee" class="btn btn-primary" data-toggle="modal" 
                                                        data-target="#editModal"
                                                         id-selected="{{$employee->id}}">
                                                        <i class="fas fa-edit"></i>
                                                </button>
                                                <button type="button" id="deleteEmployee" class="btn btn-danger" 
                                                    id-selected="{{$employee->id}}">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4">
                                            <h1 class="text-center">No Employees Found</h1>
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('admin.employees.history');
    @include('admin.employees.create');
    @include('admin.employees.edit');
</div>
@endsection
