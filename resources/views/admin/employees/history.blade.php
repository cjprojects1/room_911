<!-- Modal -->
<div class="modal fade" id="historyModal" tabindex="-1" role="dialog" aria-labelledby="historyModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <h3 class="modal-title" id="historyModalTitle">
                <i class="fas fa-history"></i>
                Access History
              </h3>
              <button type="button" class="btn btn-primary" data-dismiss="modal" id="closeHistoryModal">
                <i class="fas fa-times"></i>
                Close
              </button>
            </div>
            <div class="modal-body">
                <div class="div container-fluid">
                        <div class="row justify-content center">
                            <div class="col-12 text-right">
                            <input type="text" id="filterDate">
                        </div>
                        <div class="col-12 py-4">
                              <div class="table-responsive">
                                  <table id="employeeHistoryTable" class="table table-bordered">
                                      <thead>
                                          <tr>
                                              <th scope="col">#</th>
                                              <th scope="col">Internal Number Used</th>
                                              <th scope="col">Identified Employee Name</th>
                                              <th scope="col">Access Date</th>
                                              <th scope="col">Access Result</th>
                                          </tr>
                                      </thead>
                                      <tbody>

                                      </tbody>
                                  </table>
                              </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>