<!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
              <h3 class="modal-title" id="editModalTitle">
                <i class="fas fa-edit"></i>
                Edit Employee
              </h3>
              <button type="button" class="btn btn-primary" data-dismiss="modal" id="closeEditModal">
                <i class="fas fa-times"></i>
                Close
              </button>
            </div>
            <div class="modal-body">
                <div class="div container-fluid">
                    <div class="row justify-content-center">
                        <div class="col-12 py-4">
                            <form id="editEmployee" class="text-center">
                                <div class="form-group text-left">
                                    <label for="internalNumber">Internal Number</label>
                                    <input type="number" class="form-control" name="internal_number" id="internalNumber" placeholder="Ex. 0000000000">
                                </div>
                                <div class="form-group text-left">
                                    <label for="firstName">First Name</label>
                                    <input type="text" class="form-control" name="first_name" id="firstName" placeholder="Ex. Jhon" >
                                </div>
                                <div class="form-group text-left">
                                    <label for="middleName">Middle Name</label>
                                    <input type="text" class="form-control" name="middle_name" id="middleName" placeholder="Ex. Alexander">
                                </div>
                                <div class="form-group text-left">
                                    <label for="lastName">Last Name</label>
                                    <input type="text" class="form-control" name="last_name" id="lastName" placeholder="Ex. Doe">
                                </div>
                                <div class="form-group text-left">
                                    <label for="department">Department</label>
                                    <select class="form-control" name="department" id="department">
                                        <option value="" selected disabled>Select a production Department</option>
                                        @foreach ($allDepartments as $department)
                                            <option value="{{$department->id}}">{{$department->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <button id="bneditEmployee"type="submit" class="btn btn-primary btn-lg">
                                    <i class="fas fa-edit"></i>
                                    Update
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>