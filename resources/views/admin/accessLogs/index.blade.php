@extends('layouts.app')

@section('content')
<div class="container-fluid h-100">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card mt-5 shadow rounded">
                <div class="card-header text-center p-4">
                    <h5 class="card-title font-weight-bold mb-0">
                        <i class="fas fa-history"></i>
                        General Access History
                    </h5>
                </div>
                <div class="card-body p-3">
                    <div class="table-responsive mt-3">
                        <table id="accessLogsTable" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Internal Number Used</th>
                                    <th scope="col">Identified Employee Name</th>
                                    <th scope="col">Access Date</th>
                                    <th scope="col">Access Result</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($allAccessLogs) > 0)
                                    @foreach ($allAccessLogs as $log)
                                        <tr>
                                            <td>{{$log->id}}</td>
                                            <td>{{$log->used_number}}</td>
                                            <td>{{$log->employee_name}}</td>
                                            <td>{{$log->date}}</td>
                                            <td>{{$log->status}}</td> 
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="4">
                                            <h1 class="text-center">No Access Logs Found</h1>
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection