@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card mt-5">
                <div class="card-header p-4">
                    <h5 class="card-title font-weight-bold mb-0">
                        <i class="fas fa-bars"></i>
                        Main Menu
                    </h5>
                </div>

                <div class="card-body p-5 shadow rounded">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-12 col-md-6">
                                <a href="{{ route('listEmployees') }}" class="card bg-primary text-white">
                                    <div class="card-body text-center rounded">
                                        <h1 class="font-weight-bold">
                                            <i class="fas fa-user-tie"></i>
                                        </h1>
                                        <h5 class="font-weight-bold">
                                            Employees Management
                                        </h5>
                                    </div>
                                    <div class="card-footer py-1 my-0">
                                        <p class="font-italic text-center">
                                            Enter
                                            <i class="fas fa-external-link-square-alt" ></i>
                                        </p>
                                    </div>
                                </a>
                            </div>
                            <div class="col-12 col-md-6">
                                <a href="{{route('listLogs')}}" class="card bg-primary text-white rounded">
                                    <div class="card-body text-center">
                                        <h1 class="font-weight-bold">
                                            <i class="fas fa-history"></i>
                                        </h1>
                                        <h5 class="font-weight-bold">
                                            General Access History
                                        </h5>
                                    </div>
                                    <div class="card-footer py-1 my-0">
                                        <p class="font-italic text-center">
                                            Enter
                                            <i class="fas fa-external-link-square-alt" ></i>
                                        </p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
