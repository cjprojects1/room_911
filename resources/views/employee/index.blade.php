@extends('layouts.access')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title text-center font-weight-bold">
                        <i class="fas fa-sign-in-alt"></i>
                        Access Terminal
                    </h3>
                    <h6 class="text-center">
                        Employees Room 911
                    </h6>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('accessRoom') }}">
                        @csrf
                        @if(Session::has('access-failed'))
                            <div class="alert alert-danger bg-danger text-white text-center">
                                <h4 class="font-weigth-bold">
                                   <i class="fas fa-times"></i>
                                   {{Session::get('access-failed')}}
                                </h4> 
                            </div>
                        @endif
                        @if(Session::has('access-success'))
                            <div class="alert alert-success bg-success text-white text-center">
                               <h4 class="font-weigth-bold">
                                   <i class="fas fa-check"></i>
                                   {{Session::get('access-success')}}
                                </h4> 
                            </div>
                        @endif

                        <div class="form-group row">
                            <label for="internal_number" class="col-md-4 col-form-label text-md-right">Internal Number</label>

                            <div class="col-md-6">
                                <input id="internal_number" type="password" class="form-control @error('internal_number') is-invalid @enderror" name="internal_number" required>
                                @error('internal_number')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row justify-content-center mb-0">
                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-primary btn-lg">
                                    <i class="fas fa-sign-in-alt"></i>
                                    Access to Room
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection