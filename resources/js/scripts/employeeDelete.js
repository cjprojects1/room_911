import $ from 'jquery';
import axios from '../axios';
import Swal from 'sweetalert2';


$(document).on('click', '#deleteEmployee', function () {
    let selectedEmployee = $(this).attr('id-selected');
    Swal.fire({
        title: 'Delete Employee',
        type: 'question',
        text: 'Do you want to delete this employee?',
        showCancelButton: true,
        showConfirmButton: true,
        confirmButtonColor: '#38c172',
        cancelButtonColor: '#e3342f',
        confirmButtonText: '<i class="fas fa-check"></i> Yes',
        cancelButtonText: '<i class="fas fa-times"></i> No'
    }).then(result => {
        if (result.value) {
            axios.get(`/employees/delete/${selectedEmployee}`).then(resp => {
                let { success } = resp.data;
                if (success) {
                    Swal.fire({
                        title: 'Employee deleted',
                        type: 'success',
                        text: 'The employee has been deleted and no longer have access to room 911',
                        onAfterClose: () => {
                            location.reload();
                        }
                    });
                } else {
                    Swal.fire({
                        title: 'Delete Failed',
                        type: 'error',
                        text: 'Could not delete this employee, try again',
                        onAfterClose: () => {
                            location.reload();
                        }
                    })
                }
            }).catch(err => console.error(err));
        }
    }).catch(err => console.error(err));
});