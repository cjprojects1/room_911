import $ from 'jquery';
import axios from '../axios';
import Lightpick from  'lightpick';



$(document).on('click', '#historyEmployee', function() {
    let filter = new Lightpick({
        field: document.getElementById('filterDate'),
        singleDate: false,
        numberOfColumns: 3,
        numberOfMonths: 3
    })

    let employeeSelected = $(this).attr('id-selected');
    axios.get(`/employees/history/${employeeSelected}`).then(resp => {
        let { history } = resp.data;
        let historyData = [];
        history.forEach(record => {
            historyData.push([
                record.id,
                record.used_number,
                record.employee_name,
                record.date,
                record.status
            ]);
        });
        let drawedTable = $('#employeeHistoryTable').DataTable({
            data: historyData,
            lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            pageLength: 5,
            responsive: false,
            language: {
                search: 'Search by # / Internal Number / Full Name / Access Date:'
            }
        });

        $('#historyModal').on('hidden.bs.modal', function () {
            drawedTable.destroy();
        })
    }).catch(err => console.error(err))

    $('#filterDate').change(function () {
        let start = filter.getStartDate().format('YYYY-MM-DD HH:mm:ss').toString() || null;
        let end = filter.getEndDate().format('YYYY-MM-DD HH:mm:ss').toString() || null;
        if (start !== null && end != + null) {
            axios.post(`/employees/historyDate/${employeeSelected}`, {
                start_date: start,
                end_date: end
            }).then(resp => {
                let { history } = resp.data;
                let historyData = [];
                history.forEach(record => {
                    historyData.push([
                        record.id,
                        record.used_number,
                        record.employee_name,
                        record.date,
                        record.status
                    ]);
                });
                let drawedTable = $('#employeeHistoryTable').DataTable({
                    data: historyData,
                    lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
                    pageLength: 5,
                    responsive: false,
                    language: {
                        search: 'Search by # / Internal Number / Full Name / Access Date:'
                    }
                });

                $('#historyModal').on('hidden.bs.modal', function () {
                    drawedTable.destroy();
                })
            }).catch(err => console.error(err))
        }
    });
});
    