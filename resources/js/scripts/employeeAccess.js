import $ from 'jquery';
import axios from '../axios';
import Swal from 'sweetalert2';

$(document).on('mouseover', '#lockEmployee',function () {
    $(this).removeClass('btn-danger').addClass('btn-success');
    $(this).find('i').removeClass('fa-lock').addClass('fa-lock-open');
});

$(document).on('mouseout', '#lockEmployee', function () {
    $(this).removeClass('btn-success').addClass('btn-danger');
    $(this).find('i').removeClass('fa-lock-open').addClass('fa-lock');
});

$(document).on('mouseover', '#unlockEmployee', function () {
    $(this).removeClass('btn-success').addClass('btn-danger');
    $(this).find('i').removeClass('fa-lock-open').addClass('fa-lock');
});

$(document).on('mouseout', '#unlockEmployee', function () {
    $(this).removeClass('btn-danger').addClass('btn-success');
    $(this).find('i').removeClass('fa-lock').addClass('fa-lock-open');
});


$(document).on('click', '#lockEmployee', function () {
    let selectedEmployee = $(this).attr('id-selected');
    Swal.fire({
        title: 'Disable Access',
        type: 'question',
        text: 'Do you want to disable access to room 911 for this employee?',
        showCancelButton: true,
        showConfirmButton: true,
        confirmButtonColor: '#38c172',
        cancelButtonColor: '#e3342f',
        confirmButtonText: '<i class="fas fa-check"></i> Yes',
        cancelButtonText: '<i class="fas fa-times"></i> No'
    }).then(result => {
        if(result.value){
            axios.get(`/employees/disableAccess/${selectedEmployee}`).then(resp => {
                let {success } = resp.data;
                if(success){
                    Swal.fire({
                        title: 'Employee disabled',
                        type: 'success',
                        text: 'The employee has no longer have access to room 911',
                        onAfterClose: () => {
                            location.reload();
                        }
                    });
                } else {
                    Swal.fire({
                        title: 'Disable Failed',
                        type: 'error',
                        text: 'Could not disable access for this employee, try again',
                        onAfterClose: () => {
                            location.reload();
                        }
                    })
                }
            }).catch(err => console.error(err));
        }
    }).catch(err => console.error(err));
});

$(document).on('click', '#unlockEmployee', function () {
    let selectedEmployee = $(this).attr('id-selected');
    Swal.fire({
        title: 'Enable Access',
        type: 'question',
        text: 'Do you want to enable access to room 911 for this employee?',
        showCancelButton: true,
        showConfirmButton: true,
        confirmButtonColor: '#38c172',
        cancelButtonColor: '#e3342f',
        confirmButtonText: '<i class="fas fa-check"></i> Yes',
        cancelButtonText: '<i class="fas fa-times"></i> No'
    }).then(result => {
        if (result.value) {
            axios.get(`/employees/enableAccess/${selectedEmployee}`).then(resp => {
                let { success } = resp.data;
                if (success) {
                    Swal.fire({
                        title: 'Employee enabled',
                        type: 'success',
                        text: 'The employee now has access to room 911',
                        onAfterClose: () => {
                            location.reload();
                        }
                    });
                } else {
                    Swal.fire({
                        title: 'Enable Failed',
                        type: 'error',
                        text: 'Could not Enable access for this employee, try again',
                        onAfterClose: () => {
                            location.reload();
                        }
                    })
                }
            }).catch(err => console.error(err));
        }
    }).catch(err => console.error(err));
});