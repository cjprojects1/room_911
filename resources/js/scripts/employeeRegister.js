import $ from 'jquery';
import axios from '../axios';
import Swal from 'sweetalert2';
import _ from 'lodash';

$('#registerEmployee').submit(event => {
    event.preventDefault();
    let internal_number = $('#internalNumber').val();
    let first_name = $('#firstName').val();
    let middle_name = $('#middleName').val();
    let last_name = $('#lastName').val();
    let department = $('#department').val();

    axios.post('/employees/create', {
        internal_number: internal_number,
        first_name: first_name,
        middle_name: middle_name,
        last_name: last_name,
        department: department
    }).then(resp => {
        let { success } = resp.data;
        if (success){
            Swal.fire({
                title: 'Successful Registration',
                type: 'success',
                text: 'The employee was registered successfully',
                onClose: () => {
                    location.reload();
                }
            });
        } else {
            let { errorBag } = resp.data;
            if(errorBag !== null && errorBag !== undefined){
                let errors = '';

                _.forEach( errorBag , ( error, input) => {
                    $(`[name="${input}"]`).addClass('is-invalid');
                    errors = errors + `${error[0]} \n`;
                });

                $('#createModal').modal('dispose');
                Swal.fire({
                    title: 'Registration Failed',
                    type: 'error',
                    text: `Solve the following errors: \n ${errors}`
                });
            } else {
                Swal.fire({
                    title: 'Registration Failed',
                    type: 'error',
                    text: 'Could not complete employee registration',
                    onClose: () => {
                        location.reload();
                    }
                });
            }
        }
    }).catch(err => {
        console.error(err)
    });

    $("#registerEmployee").on("change", " :input", function () {
        $(this).removeClass('is-invalid');
    });
})