import $ from 'jquery';
import axios from '../axios';
import Swal from 'sweetalert2';
import _ from 'lodash';

let selectedEmployee = 0;
$(document).on('click', '#updateEmployee', function () {
    selectedEmployee = $(this).attr('id-selected');
    axios.get(`/employees/edit/${selectedEmployee}`).then(resp => {
        let { employee } = resp.data;
        if(employee != null) {
            $('#editEmployee #internalNumber').val(employee.internal_number);
            $('#editEmployee #firstName').val(employee.first_name);
            $('#editEmployee #middleName').val(employee.middle_name);
            $('#editEmployee #lastName').val(employee.last_name);
            $('#editEmployee #department').val(employee.department_id);
        }
    }).catch(err => { console.error(err)})
})

$('#editEmployee').submit(event => {
    event.preventDefault();
    let internal_number = $('#editEmployee #internalNumber').val();
    let first_name = $('#editEmployee #firstName').val();
    let middle_name = $('#editEmployee #middleName').val();
    let last_name = $('#editEmployee #lastName').val();
    let department = $('#editEmployee #department').val();

    axios.post(`/employees/update/${selectedEmployee}`, {
        internal_number: internal_number,
        first_name: first_name,
        middle_name: middle_name,
        last_name: last_name,
        department: department
    }).then(resp => {
        let { success } = resp.data;
        if (success) {
            Swal.fire({
                title: 'Successful Edition',
                type: 'success',
                text: 'The employee was updated successfully',
                onClose: () => {
                    location.reload();
                }
            });
        } else {
            let { errorBag } = resp.data;
            if (errorBag !== null && errorBag !== undefined) {
                let errors = '';

                _.forEach(errorBag, (error, input) => {
                    $(`#editEmployee > [name="${input}"]`).addClass('is-invalid');
                    errors = errors + `${error[0]} \n`;
                });

                $('#editModal').modal('dispose');
                Swal.fire({
                    title: 'Edition Failed',
                    type: 'error',
                    text: `Solve the following errors: \n ${errors}`
                });
            } else {
                Swal.fire({
                    title: 'Edition Failed',
                    type: 'error',
                    text: 'Could not complete employee update',
                    onClose: () => {
                        location.reload();
                    }
                });
            }
        }
    }).catch(err => {
        console.error(err)
    });

    $("#editEmployee").on("change", " :input", function () {
        $(this).removeClass('is-invalid');
    });
});

