import 'bootstrap';
import $ from 'jquery';
import axios from './axios';
import _ from 'lodash';

window.$ = $;
window.axios = axios;
window._ = _;

import 'datatables.net-bs4';
import 'datatables.net-responsive-bs4';
import 'datatables.net-select-bs4';
import './scripts/employeeHistory';
import './scripts/employeeAccess';
import './scripts/employeeRegister';
import './scripts/employeeUpdate';
import './scripts/employeeDelete';

$(document).ready(_ => {
    $('#employeeTable').DataTable({
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        pageLength: 5,
        responsive: false,
        language: {
            search: 'Search by # / Department / Any Name / Last Access:'
        }
    });

    $('#accessLogsTable').DataTable({
        lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        pageLength: 5,
        responsive: false,
        language: {
            search: 'Search by # / Internal Number / Full Name / Access Date:'
        }
    });
});
