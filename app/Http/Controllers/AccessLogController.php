<?php

namespace App\Http\Controllers;

use App\Models\AccessLog;
use App\Models\Employee;
use App\Models\Status;
use Illuminate\Http\Request;

class AccessLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accessLogs = AccessLog::all();

        foreach ($accessLogs as $log) {
            $log->status = $log->status()->first()->name;
            $log->date = $log->created_at->format('l jS \\of F Y h:i:s A');
        }

        $data = ['allAccessLogs' => $accessLogs];
        return view('admin.accessLogs.index', $data);
    }

    public function home()
    {
        return view('employee.index');
    }

    public function store(Request $request)
    {
        try {
            $request->validate([
                'internal_number' => 'required|numeric',
            ]);
            $request->session()->forget(['access-success', 'access-failed']);

            $accesLog = new AccessLog();
            $enabledID = Status::where('name', 'Enabled')->pluck('id')->first();
            $accessGrantedID = Status::where('name', 'Authorized')->pluck('id')->first();
            $accessFailedID = Status::where('name', 'Unauthorized')->pluck('id')->first();
            $accessNotRecognized = Status::where('name', 'Not Registered')->pluck('id')->first();
            $number = $request->internal_number;
            if (Employee::where('internal_number', $number)->exists()) {
                $selectedEmployee = Employee::where('internal_number', $number)->first();
                $fullname = implode(' ', [$selectedEmployee->first_name, $selectedEmployee->middle_name, $selectedEmployee->last_name]);
                if ($selectedEmployee->status_id == $enabledID) {
                    $accesLog->used_number = $request->internal_number;
                    $accesLog->employee_name = $fullname;
                    $accesLog->status_id = $accessGrantedID;
                    if ($accesLog->save()) {
                        $request->session()->flash('access-success', 'Access Granted!');
                    }
                } else {
                    $accesLog->used_number = $request->internal_number;
                    $accesLog->employee_name = $fullname;
                    $accesLog->status_id = $accessFailedID;
                    if ($accesLog->save()) {
                        $request->session()->flash('access-failed', 'Access Denied!');
                    }
                }
            } else {
                $accesLog->used_number = $request->internal_number;
                $accesLog->status_id = $accessNotRecognized;
                if ($accesLog->save()) {
                    $request->session()->flash('access-failed', 'Access Denied!');
                }
            }
            return redirect()->route('homeEmployee');
        } catch (\Exception $e) {
            return redirect()->route('homeEmployee');
            report($e);
        }
    }
}
