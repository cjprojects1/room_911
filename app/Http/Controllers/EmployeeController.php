<?php

namespace App\Http\Controllers;

use App\Models\Status;
use App\Models\Employee;
use App\Models\AccessLog;
use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::withoutTrashed()->get();
        $departments = Department::all();
        $enabledID = Status::where('name', 'Enabled')->pluck('id')->first();
        foreach ($employees as $employee) {
            $employee->department = $employee->department->name;
            $employee->countAccess = $employee->countAccess();
            $hasAccess = $employee->lastAccess()->first() != null;
            $employee->lastAccess = $hasAccess ? $employee->lastAccess()->first()->created_at->format('l jS \\of F Y h:i:s A') : 'Never Accessed';
            $employee->canAccess = $employee->status_id == $enabledID;
        }
        $data = ['allEmployees' => $employees, 'allDepartments' => $departments];
        return view('admin.employees.index', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'internal_number' => 'bail|required|unique:employees|numeric',
                'first_name' => 'bail|required|alpha',
                'middle_name' => 'bail|required|alpha',
                'last_name' => 'bail|required|alpha',
                'department' => 'bail|required|numeric',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'success' => false,
                    'errorBag' => $validator->getMessageBag()->toArray()
                ]);
            } else {
                $disabledID = Status::where('name', 'Disabled')->pluck('id')->first();
                $employee = new Employee();
                $employee->internal_number = $request->internal_number;
                $employee->first_name = $request->first_name;
                $employee->middle_name = $request->middle_name;
                $employee->last_name = $request->last_name;
                $employee->department_id = $request->department;
                $employee->status_id = $disabledID;
                if ($employee->save()) {
                    return response()->json(['success' => true]);
                } else {
                    return response()->json(['success' => false]);
                }
            }
        } catch (\Exception $e) {
            report($e);
            return response()->json(['success' => false]);
        }
    }

    /**
     * Display the specified resource history.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            $employeeNumber = Employee::find($id)->internal_number;
            $historyList = AccessLog::where('used_number', $employeeNumber)->get();
            foreach ($historyList as $historyRecord) {
                $historyRecord->status = $historyRecord->status()->first()->name;
                $historyRecord->date = $historyRecord->created_at->format('l jS \\of F Y h:i:s A');
            }
            return response()->json(['history' => $historyList]);
        } catch (\Exception $e) {
            report($e);
            return response()->json(['history' => NULL]);
        }
    }

    /**
     * Display the specified resource history.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showFilter(Request $request, $id)
    {
        try {
            $startDate = $request->start_date;
            $endDate = $request->end_date;
            $employeeNumber = Employee::find($id)->internal_number;
            $historyList = AccessLog::where('used_number', $employeeNumber)
                ->whereBetween('created_at', [$startDate, $endDate])
                ->get();
            foreach ($historyList as $historyRecord) {
                $historyRecord->status = $historyRecord->status()->first()->name;
                $historyRecord->date = $historyRecord->created_at->format('l jS \\of F Y h:i:s A');
            }
            return response()->json(['history' => $historyList]);
        } catch (\Exception $e) {
            report($e);
            return response()->json(['history' => NULL]);
        }
    }

    /**
     * Enable access for the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function enableAccess($id)
    {
        try {
            $enabledID = Status::where('name', 'Enabled')->pluck('id')->first();
            $employee = Employee::find($id);
            $employee->status_id = $enabledID;
            if ($employee->save()) {
                return response()->json(['success' => true]);
            } else {
                return response()->json(['success' => false]);
            }
        } catch (\Exception $e) {
            report($e);
            return response()->json(['success' => false]);
        }
    }

    /**
     * Disable access for the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function disableAccess($id)
    {
        try {
            $disabledID = Status::where('name', 'Disabled')->pluck('id')->first();
            $employee = Employee::find($id);
            $employee->status_id = $disabledID;
            if ($employee->save()) {
                return response()->json(['success' => true]);
            } else {
                return response()->json(['success' => false]);
            }
        } catch (\Exception $e) {
            report($e);
            return response()->json(['success' => false]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $employee = Employee::find($id);
            return response()->json(['employee' => $employee]);
        } catch (\Exception $e) {
            report($e);
            return response()->json(['employee' => NULL]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $validator = Validator::make($request->all(), [
                'internal_number' => 'bail|required|numeric',
                'first_name' => 'bail|required|alpha',
                'middle_name' => 'bail|required|alpha',
                'last_name' => 'bail|required|alpha',
                'department' => 'bail|required|numeric',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'success' => false,
                    'errorBag' => $validator->getMessageBag()->toArray()
                ]);
            } else {
                $employee = Employee::find($id);
                $employee->internal_number = $request->internal_number;
                $employee->first_name = $request->first_name;
                $employee->middle_name = $request->middle_name;
                $employee->last_name = $request->last_name;
                $employee->department_id = $request->department;
                if ($employee->save()) {
                    return response()->json(['success' => true]);
                } else {
                    return response()->json(['success' => false]);
                }
            }
        } catch (\Exception $e) {
            report($e);
            return response()->json(['success' => false]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $disabledID = Status::where('name', 'Disabled')->pluck('id')->first();
            $employee = Employee::find($id);
            $employee->status_id = $disabledID;
            $employee->delete();
            if ($employee->trashed()) {
                return response()->json(['success' => true]);
            } else {
                return response()->json(['success' => false]);
            }
        } catch (\Exception $e) {
            report($e);
            return response()->json(['success' => false]);
        }
    }
}
