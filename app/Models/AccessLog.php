<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AccessLog extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'access_logs';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'used_number', 'employee_name', 'status_id'
    ];


    public function status()
    {
        return $this->belongsTo(Status::class);
    }
}
