<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'employees';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'internal_number', 'first_name', 'middle_name', 'last_name', 'status_id', 'department_id'
    ];

    public function lastAccess()
    {
        $employeeAccess = AccessLog::where('used_number', $this->internal_number)
            ->latest();
        return $employeeAccess;
    }

    public function countAccess()
    {
        $employeeTotal = AccessLog::where('used_number', $this->internal_number)
            ->get();
        return count($employeeTotal);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }
}
