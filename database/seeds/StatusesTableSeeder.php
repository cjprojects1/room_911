<?php

use Illuminate\Database\Seeder;
use App\Models\Status;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status_list = ['Authorized', 'Enabled', 'Disabled', 'Unauthorized', 'Not Registered'];

        foreach ($status_list as $name) {
            $status = new Status();
            $status->name = $name;
            $status->save();
        }
    }
}
