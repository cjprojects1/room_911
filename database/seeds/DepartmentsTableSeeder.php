<?php

use Illuminate\Database\Seeder;
use App\Models\Department;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $department_list = ['Department A', 'Department B', 'Department C', 'Department D'];
        foreach ($department_list as $d) {
            $department = new Department();
            $department->name = $d;
            $department->save();
        }
    }
}
