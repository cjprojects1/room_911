<?php

use Illuminate\Database\Seeder;

use App\Models\AccessLog;
use App\Models\Employee;
use App\Models\Status;
use Faker\Factory as Faker;

class AccessLogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $enabledID = Status::where('name', 'Enabled')->pluck('id')->first();
        $accessGrantedID = Status::where('name', 'Authorized')->pluck('id')->first();
        $accessFailedID = Status::where('name', 'Unauthorized')->pluck('id')->first();
        $internalNumbers = Employee::pluck('internal_number');
        $faker = Faker::create();

        foreach ($internalNumbers as $number) {
            $selectedEmployee = Employee::where('internal_number', $number)->first();
            $fullname = implode(' ', [$selectedEmployee->first_name, $selectedEmployee->middle_name, $selectedEmployee->last_name]);
            $resultOfAccess = $selectedEmployee->status_id == $enabledID ? $accessGrantedID : $accessFailedID;
            for ($i = 0; $i < $faker->numberBetween(2, 10); $i++) {
                $AccessLog = new AccessLog();
                $AccessLog->used_number = $number;
                $AccessLog->employee_name = $fullname;
                $AccessLog->status_id = $resultOfAccess;
                $AccessLog->save();
                $AccessLog->created_at = $AccessLog->created_at->addHours($faker->numberBetween(0, 72));
                $AccessLog->save();
            }
        }
    }
}
