<?php

use Illuminate\Database\Seeder;
use App\Models\Employee;
use App\Models\Status;
use App\Models\Department;
use Faker\Factory as Faker;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $enabledID = Status::where('name', 'Enabled')->pluck('id')->first();
        $disabledID = Status::where('name', 'Disabled')->pluck('id')->first();
        $departmentIDS = Department::pluck('id');
        $faker = Faker::create();
        for ($i = 0; $i < 10; $i++) {
            $employee = new Employee();
            $employee->internal_number = $faker->unique()->randomNumber(8);
            $employee->first_name = $faker->firstName();
            $employee->middle_name = $faker->firstName();
            $employee->last_name = $faker->lastName();
            $employee->status_id = $i % 2 == 0 ? $enabledID : $disabledID;
            $employee->department_id = $faker->randomElement($departmentIDS);
            $employee->save();
        }
    }
}
