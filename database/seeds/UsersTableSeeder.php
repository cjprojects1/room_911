<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Models\Status;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $enabledID = Status::where('name', 'Enabled')->pluck('id')->first();
        $faker = Faker::create();

        // Default User
        $user = new User();
        $user->name = 'Admin';
        $user->password = Hash::make('admin_911_2019');
        $user->status_id = $enabledID;
        $user->save();


        // Random Users
        for ($i = 0; $i < 5; $i++) {
            $user = new User();
            $user->name = $faker->unique()->userName;
            $user->password = Hash::make('admin_911_2019');
            $user->status_id = $enabledID;
            $user->save();
        }
    }
}
