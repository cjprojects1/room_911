<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes(['register' => false, 'reset' => false]);

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/logout', function () {
    Auth::logout();
    return redirect()->route('login');
});

Route::group(['prefix' => 'history'], function () {
    Route::get('list', 'AccessLogController@index')->name('listLogs');
});

Route::get('access/room/911', 'AccessLogController@home')->name('homeEmployee');
Route::post('access/room/employee', 'AccessLogController@store')->name('accessRoom');

Route::group(['prefix' => 'employees', 'middleware' => ['auth', 'admin']], function () {
    Route::get('list', 'EmployeeController@index')->name('listEmployees');
    Route::get('history/{id}', 'EmployeeController@show')->name('getEmployeeHistory');
    Route::post('historyDate/{id}', 'EmployeeController@showFilter')->name('getEmployeeHistoryDate');
    Route::get('enableAccess/{id}', 'EmployeeController@enableAccess')->name('enableEmployeeAccess');
    Route::get('disableAccess/{id}', 'EmployeeController@disableAccess')->name('disableEmployeeAccess');
    Route::post('create', 'EmployeeController@store')->name('registerEmployee');
    Route::get('edit/{id}', 'EmployeeController@edit')->name('editEmployee');
    Route::post('update/{id}', 'EmployeeController@update')->name('updateEmployee');
    Route::get('delete/{id}', 'EmployeeController@destroy')->name('deleteEmployee');
});
